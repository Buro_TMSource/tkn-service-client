# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2018-05-22
### Released
- Basic configuration
- Adds QR code generation foundations. Release increment pending until finish process with database support
## [1.0.1] - 2018-05-28
-Database support for QR code generation