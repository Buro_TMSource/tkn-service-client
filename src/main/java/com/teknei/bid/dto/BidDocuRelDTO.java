package com.teknei.bid.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class BidDocuRelDTO implements Serializable {

    @NonNull
    private Long id;
    @NonNull
    private String code;
    @NonNull
    private String desc;
    @NonNull
    private Boolean front;
    @NonNull
    private Boolean back;
    private List<BidDocuRelDTO> children;

}